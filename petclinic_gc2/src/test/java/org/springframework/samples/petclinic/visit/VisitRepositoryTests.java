package org.springframework.samples.petclinic.visit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.time.Month;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.samples.petclinic.owner.Owner;
import org.springframework.samples.petclinic.owner.OwnerRepository;
import org.springframework.samples.petclinic.owner.Pet;
import org.springframework.samples.petclinic.owner.PetRepository;
import org.springframework.samples.petclinic.owner.PetType;
import org.springframework.samples.petclinic.vet.Specialty;
import org.springframework.samples.petclinic.vet.Vet;
import org.springframework.samples.petclinic.vet.VetRepository;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest(includeFilters = @ComponentScan.Filter(Repository.class))
public class VisitRepositoryTests {

	// Tengo que probar el método findById de Visit Repository
	private Visit visit;
	private Vet vet;
	private Pet pet;
	private Owner owner;

	@Autowired
	private VisitRepository visits;
	@Autowired
	private PetRepository pets;
	@Autowired
	private OwnerRepository owners;
	@Autowired
	private VetRepository vets;
	/////////////////////////////////////////////////////////
	// mascota a dueño visita a mascota y veterinario
	@Before
	public void init() {
		if (this.pet==null) {

			pet = new Pet();

			pet.setName("nombre");

			LocalDate fechaA = LocalDate.of(2005, 2, 1);
			pet.setBirthDate(fechaA);

			pet.setComments("Comentario");

			if (this.owner==null) {
				owner = new Owner();
				owner.setFirstName("Geormig");
				owner.setLastName("Frankmig");
				owner.setAddress("110 W. Libert");
				owner.setCity("Madison");
				owner.setTelephone("608678900");
			}
			this.owners.save(owner);


			PetType cat = new PetType();
			cat.setId(1);
			cat.setName("cat");
			pet.setType(cat);
			pet.setWeight(2);


			owner.addPet(pet);
			this.pets.save(pet);

			if (this.visit==null) {

				if (this.vet==null) {
					vet = new Vet ();
					vet.setFirstName("nombre1");
					vet.setLastName("apellido");
					vet.setHomeVisits(false);

					Collection <Specialty> vetSpec = this.vets.findSpecialties();

					if (!vetSpec.isEmpty()) {    
						for (Specialty spec : vetSpec) {
							vet.addSpecialty(spec);
						}    
					}   

					this.vets.save(vet);
				}

				LocalDate fechaVisita = LocalDate.of(2018, 1, 26);
				visit = new Visit ();
				visit.setDate(fechaVisita);
				visit.setDescription("descripcion visita");
				visit.setPetId(pet.getId());
				visit.setVet(vet);

				this.visits.save(visit);
			}

		}
	}
	///////////////////////////////////////////
	@Test
	public void testFindById() {
		Visit visitFindById = this.visits.findById(visit.getId());
		//
		assertNotNull(visitFindById.getDate());
		assertEquals(visitFindById.getDate(), visit.getDate());	

		assertNotNull(visitFindById.getDescription());
		assertEquals(visitFindById.getDescription(), visit.getDescription());

		assertNotNull(visitFindById.getId());
		assertEquals(visitFindById.getId(), visit.getId());

		assertNotNull(visitFindById.getPetId());
		assertEquals(visitFindById.getPetId(), visit.getPetId());

		assertNotNull(visitFindById.getVet());
		assertEquals(visitFindById.getVet(), visit.getVet());

		assertTrue(visits.findById(visit.getId())==visitFindById);

	}

	@Test
	public void testFindByIdNotEqual() {

		Visit visitaAux = new Visit();

		assertNull(visitaAux.getId());
		  
		LocalDate fechaVisita = LocalDate.of(2018, 2, 26);
		visitaAux = new Visit ();
		visitaAux.setDate(fechaVisita);
		visitaAux.setDescription("descripcion visita2");
		visitaAux.setPetId(pet.getId());
		visitaAux.setVet(vet);
		this.visits.save(visitaAux);

		assertNotNull(visitaAux.getId());

		Visit visitFindById = this.visits.findById(visit.getId());

		assertNotNull(visitFindById.getDate());
		assertNotEquals(visitFindById.getDate(), visitaAux.getDate());	

		assertNotNull(visitFindById.getDescription());
		assertNotEquals(visitFindById.getDescription(), visitaAux.getDescription());	

		assertNotNull(visitFindById.getId());
		assertNotEquals(visitFindById.getId(), visitaAux.getId());

		assertNotNull(visitFindById.getPetId());

		assertNotNull(visitFindById.getVet());

		assertFalse(visits.findById(visit.getId())==visitaAux);



	}
}

