package org.springframework.samples.petclinic.owner;


import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@DataJpaTest(includeFilters = @ComponentScan.Filter(Repository.class))
public class PetRepositoryTests {

	@Autowired
	private PetRepository pets;
	private Pet pet;

	@Autowired
	private OwnerRepository owners;
	private Owner george;
	
	@Before
	public void init() {
		
		if (this.pet==null) {
			
			pet = new Pet();
			
			pet.setName("nombre");
			
			LocalDate fechaA = LocalDate.of(2005, 2, 1);
			pet.setBirthDate(fechaA);
			
			pet.setComments("Comentario");
			
			if (this.george==null) {
				george = new Owner();
		        george.setFirstName("Geormig");
		        george.setLastName("Frankmig");
		        george.setAddress("110 W. Libert");
		        george.setCity("Madison");
		        george.setTelephone("608678900");
			}
			this.owners.save(george);
			pet.setOwner(george);
			
			PetType cat = new PetType();
		    cat.setId(1);
		    cat.setName("cat");
		    pet.setType(cat);
		    
		    pet.setWeight(2);
		    this.pets.save(pet);
		}
		 
	}

	@Test
	public void testDelete() {
		Pet mascotaPrueba = this.pets.findById(pet.getId());
		
		assertNotNull(mascotaPrueba.getName());
		assertEquals(mascotaPrueba.getName(), this.pet.getName());
		
		assertNotNull(mascotaPrueba.getBirthDate());
		assertEquals(mascotaPrueba.getBirthDate(), this.pet.getBirthDate());
		
		assertNotNull(mascotaPrueba.getComments());
		assertEquals(mascotaPrueba.getComments(), this.pet.getComments());
		
		assertNotNull(mascotaPrueba.getType());
		assertEquals(mascotaPrueba.getType(), this.pet.getType());
		
		assertNotNull(mascotaPrueba.getWeight());
		assertTrue(mascotaPrueba.getWeight() == this.pet.getWeight());
		
		assertNotNull(this.pet.getId());
		
	    this.pets.deletePetInfo(this.pet.getId());
	    this.pets.delete(this.pet);
		
		assertNull(this.pets.findById(pet.getId()));
	}
	
	@Test
	public void testFindPetsByName() {
		Collection<Pet> mascotasPrueba = this.pets.findPetsByName(this.pet.getName());
		
		assertTrue(mascotasPrueba.size()>=1);
		
		for (Pet petAux : mascotasPrueba) {
			
			assertNotNull(petAux.getId());
			
			if (petAux.getId() == this.pet.getId()) {
				assertNotNull(petAux.getName());
				assertEquals(petAux.getName(), this.pet.getName());
				
				assertNotNull(petAux.getBirthDate());
				assertEquals(petAux.getBirthDate(), this.pet.getBirthDate());
				
				assertNotNull(petAux.getComments());
				assertEquals(petAux.getComments(), this.pet.getComments());
				
				assertNotNull(petAux.getType());
				assertEquals(petAux.getType(), this.pet.getType());
				
				assertNotNull(petAux.getWeight());
				assertTrue(petAux.getWeight() == this.pet.getWeight());
			}
		}
	}
	
	@After
	public void finish() {
		this.pet=null;
	}
}

